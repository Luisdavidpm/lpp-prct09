
#Clase Lista_double que crea una lista doblemente enlazada enlazada
class List_double
    #principio con el apuntador del inicio de la lista
    attr_accessor :principio 
    
    #final con el apuntador del ultimo elemento de la lista
    attr_accessor :final
    
    include Enumerable
    
    
    #Initialize que pone los apuntadores del inicio y del fin a nulo indicando asi que la lista esta vacia 
    def initialize
        @principio = nil
        @final = nil
    end
    
    #Metodo Each que es necesario para que la lista doble sea enumerable
    def each
        n = @principio
        while n!=nil do
            yield n.value
            n = n.next
        end
    end
    
    #Metodo insertarp que sirve para insertar un nodo al principio
    def insertarp(nodo)
         n = Node_double.new(nodo, nil, nil)
         n.next = @principio
         n.prev = nil
         if(@final == nil) then
             @final = n
         else
             @principio.prev = n;
         end
         @principio = n
    end
    
    #Metodo insertarf que sirve para insertar un nodo al final
    def insertarf(nodo)
         n = Node_double.new(nodo, nil, nil)
         n.prev = @final
         n.next = nil

        if(@final == nil) then
            @principio = n
        else
            @final.next = n
        end
        @final = n
    end
    
    #Metodo extraerp que sirve para extraer un nodo al principio
    def extraerp
       aux = @principio
       @principio = @principio.next
       
       if(@principio == nil) then 
        @final = nil
       end
       return aux
    end
    
    #Metodo extraerf que sirve para extraer un nodo al final
    def extraerf
        aux = @final
        @final = @final.prev
        
        if(@final == nil) then
            @principio = nil
        else
            @final.next = nil
        end
        return aux
    end
    
end