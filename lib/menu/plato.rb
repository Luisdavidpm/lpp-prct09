
#Clase plato que sirve para crear un plato que posteriormente se le añadirá al menu
class Plato
    
    #Atributo descripcion que contiene la descripcion completa del menu
    attr_accessor :descripcion
    
    #Atributo porcion que contien el numero de porciones que posee el plat
    attr_accessor :porcion 
    
    #Atributo gramos que contiene le peso en gramos del plato
    attr_accessor :gramos
    
    #Initialize que crea la clase platos a partir de los parametros que conforman un plato
    def initialize(descripcion, porcion, gramos)
        @descripcion = descripcion
        @porcion = porcion
        @gramos = gramos  
    end
end