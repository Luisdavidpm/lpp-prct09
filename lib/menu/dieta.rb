
#Clase Diesta que es utilizada para crear menus dieteticos
class Dieta
    #Atributo con el titulo del menu
    attr_accessor :titulo
    
    #Atributo con el valor calorico total del menu
    attr_accessor :vct
    
    #Atributo que contiene el porcentaje ingesta calorica diario del menu
    attr_accessor :ingesta
    
    #Atributo que guarda el conjunto de platos del menu. Al crearse el menu este atributo esta inicialmente vacio y hay que meter los platos manualmente mediante el metodo setPlato
    attr_accessor :platos
    
    #Atributo con la cantidad de proteinas del menu
    attr_accessor :proteinas
    
    #Atributo con la cantidad de grasas del menu
    attr_accessor :grasas
    
    #Atributo con la cantidad de hidratos del menu
    attr_accessor :hidratos
    
    include Comparable
    
    #Initalizaer que crea una dieta en funcion de unos parametros
    
    def initialize(receta, vct, ingesta, proteinas, grasas, hidratos)
    
        @titulo = receta
        @vct = vct
        @ingesta = ingesta
        @proteinas = proteinas
        @grasas = grasas
        @hidratos = hidratos
        @platos = []
    end
    
    #Metodo <=> que sirve para que le menu sea comparable
    
    def <=>(other)
        @vct <=> other.vct
    end
    
    #Metodo == que sobreescribe el para que mire si la proteinas grasas e hidratos sean iguales
    
    def ==(other)
        if @proteinas == other.proteinas then
            if @grasas == other.grasas then
                if @hidratos == other.hidratos then
                    return true
                else
                    return false
                end
            else
                return false
            end
        else
            return false
        end
    end
    
    #Metodo para introducir platos al menu
    
    def setPlato(descripcion, porcion, gramos)
        @platos << Plato.new(descripcion, porcion, gramos)
    end
    
    #Metodo to_s que devuelve un string con el menu formateado
    
    def to_s
        s = "#{titulo} (#{ingesta}%)\n"
        platos.each do |comida|
            s << "- #{comida.descripcion}, #{comida.porcion}, #{comida.gramos} g\n" 
        end
        s << "V.C.T. | % #{vct} kcal | #{proteinas}% - #{grasas}% - #{hidratos}%"
    end
    
end

#Clase Dieta_alimento que crea un menu dietetico que además esta agrupado por grupo alimenticio

class Dieta_alimento < Dieta
    
    #Atributo con el titulo del menu
    attr_accessor :titulo
    
    #Atributo con el valor calorico total del menu
    attr_accessor :vct
    
    #Atributo que contiene el porcentaje ingesta calorica diario del menu
    attr_accessor :ingesta
    
    #Atributo que guarda el conjunto de platos del menu. Al crearse el menu este atributo esta inicialmente vacio y hay que meter los platos manualmente mediante el metodo setPlato
    attr_accessor :platos
    
    #Atributo con la cantidad de proteinas del menu
    attr_accessor :proteinas
    
    #Atributo con la cantidad de grasas del menu
    attr_accessor :grasas
    
    #Atributo con la cantidad de hidratos del menu
    attr_accessor :hidratos
    
    #Atributo con el grupo alimenticio al que pertenece el menu
    attr_accessor :grupo
    
    #Metodo Initialize que crea un menu dietetico mediante una llamada al super y además le el nuevo atributo grupo alimenticio
    
    def initialize(receta, vct, ingesta, proteinas, grasas, hidratos, platos, grupo)
        super(receta, vct, ingesta, proteinas, grasas, hidratos)
        @platos = platos
        @grupo = grupo
    end    
    
    #Metodo unico de Dieta_alimento que devuelve el grupo alimenticio al que pertece
    
    def get_grupo_alimentos
        s = @grupo
        return s
    end
end

#Clase Dieta_alimento que crea un menu dietetico que además esta agrupado por grupo edad recomendada

class Dieta_edad < Dieta
    #Atributo con el titulo del menu
    attr_accessor :titulo
    
    #Atributo con el valor calorico total del menu
    attr_accessor :vct
    
    #Atributo que contiene el porcentaje ingesta calorica diario del menu
    attr_accessor :ingesta
    
    #Atributo que guarda el conjunto de platos del menu. Al crearse el menu este atributo esta inicialmente vacio y hay que meter los platos manualmente mediante el metodo setPlato
    attr_accessor :platos
    
    #Atributo con la cantidad de proteinas del menu
    attr_accessor :proteinas
    
    #Atributo con la cantidad de grasas del menu
    attr_accessor :grasas
    
    #Atributo con la cantidad de hidratos del menu
    attr_accessor :hidratos
    
    #Atributo con el grupo alimenticio al que pertenece el menu
    attr_accessor :grupo
    
    #Metodo Initialize que crea un menu dietetico mediante una llamada al super y además le el nuevo atributo de edad recomendada
    
    def initialize(receta, vct, ingesta, proteinas, grasas, hidratos, platos, grupo)
        super(receta, vct, ingesta, proteinas, grasas, hidratos)
        @platos = platos
        @grupo = grupo
    end    
    
    #Metodo unico de Dieta_alimento que devuelve la edad recomenda al que pertece
    
    def get_grupo_edad
        s = @grupo
        return s
    end
end

