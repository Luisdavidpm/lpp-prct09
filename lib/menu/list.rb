
#Clase Lista que crea una lista simple enlazada

class List
    
    #Atribut cabecera que apunta al ultimo nodo de la lista
    attr_accessor :cabecera
    
    
    #Metodo initialize que pone la cabecera a nill
    def initialize
        @cabecera = nil
    end
    
    #Metodo que inserta un nodo
    def insertar(nodo)
        n = Node.new(nodo.value, nodo.next)
        n.next = @cabecera
        @cabecera = n
    end
    
     #Metodo que extrae un nodo
    def extraer
        aux = cabecera
        @cabecera = @cabecera.next
        return aux
        
    end
    
end
